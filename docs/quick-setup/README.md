﻿Quick Setup
===========


## Requirements

- 6 GB of RAM (for Vagrant)
- [VirtualBox](https://www.virtualbox.org/) 5.2 or above installed
- [Vagrant](https://www.vagrantup.com/) installed


## Steps

1. Open a command prompt.
2. Clone BEAR-Necessities-Market
    ```
    git clone https://gitlab.com/LibreFoodPantry/BEAR-Necessities-Market.git
    ```
3. Change into the root of the project, which was created by the previous command.
    ```
    cd BEAR-Necessities-Market
    ```
4. Use Vagrant to create a virtual machine, install all needed software, and run the virtual machine as needed.
    ```
    vagrant up
    ```
    > **Note:** This command uses the configuration given in `VagrantFile` which is in the current working directory. Since our current working directory is the root of the project, it will use the `VagrantFile` in the root of the project.

    > **Note:** This first time you run this command, it will take a long time to complete. It has to create the virtual machine, install the Ubuntu operating system, boot the virtual machine, install all the required dependencies into the virtual machine, and install this project into the virtual machine.

5. Log into the virtual machine.
   ```
   vagrant ssh
   ```

   > **Note:** This command connects your shell to the virtual machine. You are now operating inside that virtual machine. You are now working within the virtual machine's filesystem, and you are in a different working directory than you were before&mdash;***you are no longer in the root of the project***. You are running as a different user within that virtual machine. When you issue commands using this shell, you are issue commands to the virtual machine, not your native host machine. Also, you are now in a bash shell within Linux. So if you were in a Windows command-prompt before, you are now inside a Linux machine.

6. Change into the root of the project within the virtual machine.
    ```
    cd BEAR-Necessities-Market
    ```

7. Create the FLASK_APP environment variable within the virtual machine.
    ```
    export FLASK_APP=manage.py
    ```

8. ***Skip this step unless you are working on implementing the email notification system.*** Create environment variables for SendGrid integration.
    ```
    export SENDGRID_API_KEY="<your api key>"
    export SENDGRID_DEFAULT_FROM="<your default email address>"
    ```

9. Initialize flask database.
    ```
    flask db upgrade
    ```

10. Run the flask server.
    ```
    flask run --host=0.0.0.0
    ```

    > **Note:** This command will continue to run until you stop it with `CTRL+C`.

11. In your native host machine, open another command-prompt. And position it in the root of the project you cloned in step 2.

12. Connect this new command-prompt to the virtual machine.
    ```
    vagrant ssh
    ```

13. Within the virtual machine, change into the root of the project's frotend directory.
    ```
    cd BEAR-Necessities-Market/frontend
    ```

14. Start the front end server.
    ```
    sudo yarn build
    ```

    > **Note:** The first time you run this command, it will take a very long time as it needs to download all the javascript dependencies, build the front end, and then run the server.

15. Open a browser and point it to http://localhost:8000

## Turn off the virtual machine

When you are "done" and ready to shut down the virtual machine, do the following. Open a command-prompt and position it in the root of the project created in step 2 above. And then run the following command.

```
vagrant halt
```

## Suspend the virtual machine

If you would rather just put the virtual machine to sleep, open a command-prompt and position it in the root of the project created in step 2 above. And then run the following command.

```
vagrant suspend
```

## Restart the virtual machine

Open a command-prompt and position it in the root of the project created in step 2 above. And then run the following command.

```
vagrant up
```

## Git and the Virtual Machine

All git commands should be made from a command-prompt in the native host machine, and ***NOT within the virtual machine***.


---
Copyright &copy; 2019 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
